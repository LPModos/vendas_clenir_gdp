from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Vendas de Clenir - Cascavel/PR',
    author='Luiz Paulo Modos',
    license='',
)
